# Star Trek: United Federation Of Planets (logo)

I found this súper cool United Federation Of Planes logotype in a phone wallpaper long ago.
It seems to reinterpret one of the original logos, but I like it the most.

I first saw it in a wallpaper pack from Ged Maheux, co-founder of The IconFactory.

- [Star Trek: Next Gen Wallpapers](https://gedblog.com/2014/09/19/star-trek-next-gen-wallpapers-for-iphone-6/)
- [Star Trek LCARS Wallpaper 2021](https://gedblog.com/2021/03/11/star-trek-lcars-wallpaper-2021/)

Some wrongs have to be righted, tho:

- The laurels should follow the main circle path.
- Some weird empty spaces could use one or two dots here and there.

## Original | Adjustments (red)  

<img src="./assets/img/original.png" width="50%" alt="Original art: Center double-lined circle with a simulation of outer space with multiple dots and three stars. There are two sets of laurels on each side of this circle."><img src="./assets/img/adjustments.png" width="50%" alt="Red adjustments over the original piece.">

## Final art

![Final logo](./assets/img/cover.png)

You can download my modified logo in [SVG format](./dist/united-federation-of-planets.svg).

---

**Note to Ged**: I hope these adjustments do not bother you. I know that the need to modify your logo is a character fault (of mine), not a feature.
